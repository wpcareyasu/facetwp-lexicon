<?php
/*
Plugin Name: FacetWP - Lexicon
Plugin URI: http://wordpress.org/plugins/menu-item1/
Description: Adding a custom post type Lexicon and extending FacetWP functionality for it.
Author: Danny Martinez
Version: 0.1.0
Author URI: http://martinezd.com
Bitbucket Plugin URI: https://bitbucket.org/wpcareyasu/facetwp-lexicon
Bitbucket Branch: master
*/

//Create the Leixcon Post type
add_action( 'init', __NAMESPACE__ . '\\create_post_type', 0);
function create_post_type() {
  $labels = array(
    'name'               => _x( 'Lexicon', 'post type general name', 'brandbook-extras' ),
    'singular_name'      => _x( 'Definition', 'post type singular name', 'brandbook-extras' ),
    'menu_name'          => _x( 'Lexicon', 'admin menu', 'brandbook-extras' ),
    'name_admin_bar'     => _x( 'Definition', 'add new on admin bar', 'brandbook-extras' ),
    'add_new'            => _x( 'Add New', 'definition', 'brandbook-extras' ),
    'add_new_item'       => __( 'Add New Definition', 'brandbook-extras' ),
    'new_item'           => __( 'New Definition', 'brandbook-extras' ),
    'edit_item'          => __( 'Edit Definition', 'brandbook-extras' ),
    'view_item'          => __( 'View Definition', 'brandbook-extras' ),
    'all_items'          => __( 'All Lexicon', 'brandbook-extras' ),
    'search_items'       => __( 'Search Lexicon', 'brandbook-extras' ),
    'parent_item_colon'  => __( 'Parent Definition:', 'brandbook-extras' ),
    'not_found'          => __( 'No definition found.', 'brandbook-extras' ),
    'not_found_in_trash' => __( 'No definition found in Trash.', 'brandbook-extras' )
  );

  $args = array(
    'labels'             => $labels,
    'menu_icon'          => 'dashicons-format-aside',
    'description'        => __( 'Description.', 'brandbook-extras' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => get_page_uri( get_page_by_title( 'Lexicon' ) ) . '/definition' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => 5,
    'supports'           => array( 'title', 'editor', 'revisions', 'page-attributes'),
    'taxonomies'         => array( 'category', 'post_tag')
  );

  register_post_type( 'lexicon_definition', $args  );
}

register_activation_hook( __FILE__, 'my_rewrite_flush' );
function my_rewrite_flush() {
  create_post_type();
  flush_rewrite_rules();
}

add_action( 'wp_enqueue_scripts', 'facetwp_lexicon_enqueue_style' );
function facetwp_lexicon_enqueue_style() {
  wp_enqueue_style( 'facetwp_lexicon_style', plugins_url( 'lexicon.css', __FILE__ ) );
  wp_enqueue_script( 'facetwp_lexicon_script', plugins_url( 'lexicon.js', __FILE__ ), array('jquery'), false, true );
}

//FacetWP - Adding Sort Options
add_filter( 'facetwp_sort_options', 'my_facetwp_sort_options', 10, 2 );

function my_facetwp_sort_options( $options, $params ) {
    $options['last_modified'] = array(
        'label' => 'Last Updated',
        'query_args' => array(
            'orderby' => 'modified', // sort by numerical custom field
            'order' => 'DESC', // descending order
        )
    );
    $options['default']['label'] = 'Relevance';
    return $options;
}

remove_filter('the_title', 'wptexturize');

//Check if post is post_type lexicon so it can use the plugin template.
add_filter('single_template', __NAMESPACE__ . '\\lexicon_template');
function lexicon_template($single) {
    global $wp_query, $post;
    /* Checks for single template by post type */
    if ($post->post_type == "lexicon_definition"){
        if(file_exists(plugin_dir_path( __FILE__ ) . '/template-lexicon-definition.php'))
            return plugin_dir_path( __FILE__ ) . '/template-lexicon-definition.php';
    }
    return $single;
}


// Adding appropiate classes so that the correct style gets applied to the page.
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    if ( is_singular( 'lexicon_definition' ) ) {
        $classes[] = '-empty';
        $classes[] = 'sidebar-primary';
        $classes[] = 'page';
        unset( $classes[array_search('single', $classes)] );
    }
    return $classes;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

// Adding functionality so that the lexicon page is selected in the menu when viewing individual lexicon entries.
function add_current_nav_class($classes, $item) {

    // Getting the current post details
    global $post;

    // Get post ID, if nothing found set to NULL
    $id = ( isset( $post->ID ) ? get_the_ID() : NULL );

    // Checking if post ID exist and it's a lexicon page
    if (isset( $id ) && $post->post_type === 'lexicon_definition'){

        // Getting the post type of the current post
        $current_post_type = get_post_type_object(get_post_type($post->ID));

        // Getting the rewrite slug containing the post type's ancestors
        $ancestor_slug = $current_post_type->rewrite['slug'];
        $ancestor_slug = str_replace('/definition', '' , $ancestor_slug);

        // Split the slug into an array of ancestors and then slice off the direct parent.
        $ancestors = explode('/',$ancestor_slug);
        $parent = array_pop($ancestors);

        // Getting the URL of the menu item
        $menu_slug = strtolower(trim($item->url));


        // If the menu item URL contains the post type's parent
        if (!empty($menu_slug) && !empty($parent) && endsWith($menu_slug, $parent . '/')) {
            $classes[] = 'current-menu-item';
            $classes[] = 'current_page_item';
            $classes[] = 'active';
        }else{
           // If the menu item URL contains any of the post type's ancestors
            foreach ( $ancestors as $ancestor ) {
                //Only works with parents that aren't URL's, for example #parent-name ;
                if (endsWith($menu_slug, $ancestor)) {
                    $classes[] = 'current-menu-parent';
                    break;
                }
            } 
        }
    }
    // Return the corrected set of classes to be added to the menu item
    return $classes;

} add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );