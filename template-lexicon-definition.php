<?php
/**
 * Template Name: Lexicon Definition
 */

?>

<?php $lexicon_link = get_permalink( get_page_by_title( 'Lexicon' ) ); ?>
<?php while ( have_posts() ): the_post(); ?>
    <div class="page-header">
        <h1>Lexicon</h1>
    </div>
    <p class="lead">The lexicon contains definitions, preferred spelling, and usage guidelines for content produced by and about the W. P. Carey School of Business at Arizona State University. <a href="<?php echo $lexicon_link ?>"">Return to the lexicon</a> to view additional entries beyond that shown below.</p>
    <article class="definition-wrapper">
        <span class="date">Last updated: <?php echo get_the_modified_date(); ?> </span>
        <h2 class="title"><?php the_title(); ?> </h2>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <?php
            $categories = get_the_category();
            $output = '';
            if ( ! empty( $categories ) ) {
                echo '<hr><div class="row"><div class="col-sm-8"><small>Topics:</small> ';
                foreach( $categories as $category ) {
                    $output .= '<a href="'. $lexicon_link . '?fwp_categories=' . $category->slug . '" data-category="' . $category->slug . '" class="fwp-category btn btn-xs btn-default" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a> ';
                }
                echo '</small>' . $output . '</div><div class="col-sm-4 text-right"><small><button class="btn btn-default btn-sm btn-popover" rel="popover" data-title="Permalink" data-url="'. get_permalink(). '" data-trigger="focus"><i class="fa fa-link" aria-hidden="true"></i></button></small></div></div>';
            }
        ?>
    <?php ( ( current_user_can( 'edit_posts' ) )? '<span class="edit-link">' . edit_post_link() . '</span>' : '' ); ?>
    </article>
    <a href="<?php echo $lexicon_link ?>" class="btn btn-default"><i class="fa fa-chevron-left" aria-hidden="true"></i> Return to Lexicon</a>
<?php endwhile; ?> 