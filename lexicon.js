jQuery(document).ready(function($) {

    $('body').popover({
        html: true,
        title: function() {
            return 'Permalink';
        },
        content: function() {
            return '<input type="text" class="autoselect" value="' + $(this).attr('data-url') + '">';
        },
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-title"></div><div class="popover-content"></div></div>',
        placement: 'top',
        selector: '.btn-popover'
    });

    $(document.body).on('focus', '.autoselect', function(event) {
        $(this).select();
    });

    $('body').on('click', function(event) {
        $('[rel="popover"]').each(function() {
            if (!$(this).is(event.target) && $(this).has(event.target).length === 0 && $('.popover').has(event.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    //custom FacetWP function to erase all facets, but no refresh the results.
    function eraseFacets() {
        FWP.parse_facets();
        FWP.is_reset = true;
        $.each(FWP.facets, function(f) {
            FWP.facets[f] = [];
        });
        FWP.used_facets = {};
    }

    //Reset facets when switching tabs.
    $('.nav-tabs a[href="#tab-alpha"]').click(function(event) {
        if (!($.isEmptyObject(FWP.facets.search)) || FWP.extras.sort !== 'title_asc' || !($.isEmptyObject(FWP.facets.categories))) {
            FWP.reset();
        }
    });
    $('.nav-tabs li a[href="#tab-recent"]').click(function(event) {
        if (FWP.extras.sort !== 'last_modified' || (FWP.facets.post_title && FWP.facets.post_title.length) || FWP.facets.categories.length > 0 || FWP.facets.search.length > 0) {
            $('.results-meta .results-status').prepend('<div class="facetwp-loading"></div>');
            eraseFacets();
            FWP.extras.sort = 'last_modified';
            FWP.refresh();
        }
    });
    $('.nav-tabs a[href="#tab-search"]').click(function(event) {
        if (!$.isEmptyObject(FWP.facets.alpha) || !($.isEmptyObject(FWP.facets.categories))) {
            eraseFacets();
            FWP.extras.sort = 'default';
            FWP.refresh();
        } else {
            $('.facetwp-sort-select').val('default');
        }
    });
    $('.nav-tabs a[href="#tab-topics"]').click(function(event) {
        if ((FWP.facets.post_title && FWP.facets.post_title.length) || FWP.facets.search.length > 0 || !$.isEmptyObject(FWP.facets.alpha) || FWP.extras.sort !== 'title_asc') {
            eraseFacets();
            FWP.extras.sort = 'title_asc';
            FWP.refresh();
        } else {
            $('.facetwp-sort-select').val('title_asc');
        }
    });

    $(document.body).on('change', '.facetwp-search', function() {
        FWP.extras.sort = 'default';
        $('.facetwp-sort-select').val('default');
    });

    $(document.body).on('click', '.fwp-search', function(event) {
        event.preventDefault();
        eraseFacets();
        $('.facetwp-search').attr('value', $(this).attr('data-search'));
        FWP.refresh();
        jQuery('.nav-tabs a[href="#tab-search"]').tab('show');
    });

    $(document.body).on('click', '.fwp-category', function(event) {
        if(!(window.location.href.indexOf('definition') > -1)){
            event.preventDefault();
            eraseFacets();
            FWP.facets.categories = [$(this).attr('data-category')];
            FWP.refresh();
            jQuery('.nav-tabs a[href="#tab-topics"]').tab('show');
        }
    });

    $(document.body).on('change', '.facetwp-sort-select', function() {
        $('.results-meta .results-status').prepend('<div class="facetwp-loading"></div>');
    });

    $(document.body).on('change', '.facetwp-dropdown', function() {
        $('.results-meta .results-status').prepend('<div class="facetwp-loading"></div>');
    });

    $(document)
        .on('facetwp-refresh', function() {
            if (FWP.loaded) {
                $('.definition-wrapper').remove();
                var navTab = $('.nav-tabs').offset().top - 30;
                $('html, body').animate({ scrollTop: navTab }, 500);
            } else {
                //if facets.search isn't empty, show search tab.
                if (!$.isEmptyObject(FWP.facets.search)) {
                    jQuery('.nav-tabs a[href="#tab-search"]').tab('show');
                    //if search is active then make sure sort is set to default;
                    if (FWP.extras.sort !== 'default') {
                        FWP.extras.sort = 'default';
                    }
                } else if (FWP.extras.sort === 'last_modified') {
                    jQuery('.nav-tabs a[href="#tab-recent"]').tab('show');
                } else {
                    FWP.extras.sort = 'title_asc';
                }
                //make sure the right tab is selected on initial load
                if (FWP.facets.post_title && FWP.facets.post_title.length) {
                    jQuery('.nav-tabs a[href="#tab-search"]').tab('show');
                } else if (FWP.facets.categories.length) {
                    jQuery('.nav-tabs a[href="#tab-topics"]').tab('show');
                }
            }
        })
        .on('facetwp-loaded', function() {
            $('.results-meta .results-status .facetwp-loading').remove();
            if ($('.facetwp-sort-select').val() === FWP.extras.sort) {
                $('.facetwp-sort-select').val(FWP.extras.sort);
            }
        });
});
